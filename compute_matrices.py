#!/bin/env python3
from nltk.corpus import brown
from collections import Counter

# CONSTANTS
N = 1000
CONTEXT = 50
# N = 3
# CONTEXT = 2


class Context:
    """ simulates queue with the ability
        to be iterated through without losing elements """
    def __init__(self):
        self.words = []
        self.curr = 0

    def pop(self):
        w = self.words[0]
        self.words = self.words[1:]
        return w

    def push(self, val):
        self.words.append(val)

    def __len__(self):
        return len(self.words)

    def __iter__(self):
        return self

    def __next__(self):
        self.curr += 1
        if self.curr > len(self.words):
            self.curr = 0
            raise StopIteration
        return self.words[self.curr-1]


def get_counter_words():
    """ orders all words from the corpus and
        returns counter of all words and their numbers of occurencies:
            * { word -> position }"""

    freqs = Counter([x for x in map(str.lower, brown.words())
                     if 'a' <= x[0] <= 'z'])

    return freqs


def compute_matrix():
    """ returns matrix of 2NxN as defined in the article """
    counter_ws = get_counter_words()

    # dictionary 'word' -> position in seq ordered by frequency
    pos_ws = {w: i for (i, (w, _))
              in enumerate(counter_ws.most_common())}

    # set of N most frequent words
    top_freq = set(map(lambda a: a[0], counter_ws.most_common(N)))

    # pos_ws={'a':0, 'b':1, 'c':2, 'd':3, 'e':4}
    # top_freq=set(['a','b','c'])

    matrix = [[0 for y in range(2*N)] for x in range(len(pos_ws))]

    for p in brown.paras():
    # for p in [[['a','a','a','b','d','e','a','c']],[['c','c','a','b','c']]]:
        # creates before and after context - one by one shifts words:
        # 'before' -> 'curr_middle' -> 'after'
        before = Context()
        after = Context()
        curr_middle = ''

        # add filling at the end to process all words
        for w in sum(p, [])+['' for x in range(CONTEXT)]:
            w = w.lower()

            # skip non-words
            if len(w) == 0 or not ('a' <= w[0] <= 'z'):
                continue

            after.push(w)

            # 'after' is full - shifting can occur
            if len(after) > CONTEXT:
                # MOVE to the next word
                if curr_middle != "":
                    before.push(curr_middle)
                    if len(before) > CONTEXT:
                        before.pop()
                curr_middle = after.pop()

                # all top words in 'before' +1
                for b in before:
                    if b in top_freq:
                        matrix[
                            pos_ws[curr_middle]][
                            pos_ws[b]] += 1

                # all top words in 'after' +1
                for a in after:
                    if a in top_freq:
                        matrix[
                            pos_ws[curr_middle]][
                            N+pos_ws[a]] += 1

    return matrix
