#!/bin/env python3
# import nltk
# nltk.download()
import compute_matrices
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from statistics import mean
from statistics import pstdev
from pprint import pprint
import random
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import cosine_distances


def ncs(index1, index2, matrix):
    # correlate semantic vectors
    # vec_sample = random.sample(transformed, len(transformed))
    cos1 = cosine_similarity(vec_sample, [transformed[index1]]).reshape(1, -1)
    cos2 = cosine_similarity(vec_sample, [transformed[index2]]).reshape(1, -1)

    m1 = mean(cos1[0])
    d1 = pstdev(cos1[0])
    m2 = mean(cos2[0])
    d2 = pstdev(cos2[0])

    cos_s = cosine_similarity([transformed[index1]], [transformed[index2]])[0][0]
    r1 = (cos_s-m1)/d1
    r2 = (cos_s-m2)/d2
    print()
    print(r1, r2)

    return min(r1, r2)


def center_matrix(matrix):
    """ takes matrix, computes mean and deviation of the columns
        of the first N rows, and centres all rows with the foll. formula:
            (val - mean)/dev

        returns changed matrix"""

    submatrixT = list(zip(*matrix[:compute_matrices.N]))

    means = [mean(column) for column in submatrixT]
    devs = [pstdev(column) for column in submatrixT]

    # there has to be list around map - otherwise m,d have wrong values!!
    matrix = [list(map((lambda t: (t[0]-t[1])/t[2]), zip(row, means, devs)))
              for row in matrix]

    return matrix


# obtaining matrix
matrix = compute_matrices.compute_matrix()
matrix = center_matrix(matrix)
COMPONENTS = 300
dim_red = PCA(n_components=COMPONENTS)
dim_red.fit(matrix[:compute_matrices.N])
transformed = list(dim_red.transform(matrix))


vec_sample = random.sample(transformed, 200)

print(ncs(217, 251, transformed))  # look, looked
print(ncs(218, 251, transformed))  # asked, looked
print(ncs(340, 931, transformed))  # car, cars 5.6
print(ncs(340, 8777, transformed))  # car, cares -0.14
print(ncs(8416, 3474, transformed))  # ally, allies 6.5
print(ncs(8416, 35, transformed))  # ally, all -1.3
